package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Map;

@Component
public class CartServiceImpl implements CartService{

    @Autowired
    private Map<Integer, Item> catalogItems;

    @Autowired
    private CartRepository cartRepository;

    @Value("${contactEmail}")
    private String contactEmail;

    @Value("${onlineRetailer.salesTaxRate}")
    private double salesTaxRate;

    @Value("${onlineRetailer.deliveryCharge.normal}")
    private double normalDelivery;

    @Value("${onlineRetailer.deliveryCharge.threshold}")
    private int threshold;

    public String getContactEmail() {
        return contactEmail;
    }

    public double getSalesTaxRate() {
        return salesTaxRate;
    }

    public double getNormalDelivery() {
        return normalDelivery;
    }

    public int getThreshold() {
        return threshold;
    }

    @Override
    public void addItemToCart(int id, int quantity){
        if(catalogItems.containsKey(id)){
            cartRepository.add(id, quantity);
        }
    }
    @Override
    public void removeItemFromCart(int id){
        cartRepository.remove(id);
    }
    @Override
    public Map<Integer, Integer> getAllItemsInCart(){
        return cartRepository.getAll();
    }
    @Override
    public double calculateCartCost(){
        double cost = 0;
        Map<Integer, Integer> cart = cartRepository.getAll();
        for(Map.Entry<Integer, Integer> entry : cart.entrySet()){
            int id = entry.getKey();
            int quantity = entry.getValue();
            cost += catalogItems.get(id).getPrice();
        }
        return cost;
    }
}
