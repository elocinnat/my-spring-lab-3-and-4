package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

@RestController
@RequestMapping("/catalog")
@CrossOrigin
public class CatalogRestController {

    @Autowired
    private Map<Integer, Item> catalog;

    @GetMapping(produces = {"application/json", "application/xml"})
    public ResponseEntity<Collection<Item>> getAllCatalogItems(){
        Collection<Item> result = catalog.values();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping(value = "/{id}", produces = {"application/json", "application/xml"})
    public ResponseEntity<Item> getCatalogItem(@PathVariable int id){
        Item item = catalog.get(id);
        if(item != null){
            return ResponseEntity.ok().body(item);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping(consumes={"application/json","application/xml"}, produces = {"application/json", "application/xml"})
    public ResponseEntity<Item> insertCatalogItem(@RequestBody Item item){
        catalog.put(item.getId(), item);
        URI uri = URI.create("/catalog/" + item.getId());
        return ResponseEntity.created(uri).body(item);
    }

    @PutMapping(value = "/{id}", consumes={"application/json","application/xml"})
    public ResponseEntity updateCatalogItem(@PathVariable int id, @RequestBody Item item){
        if(!catalog.containsKey(id)){
            return ResponseEntity.notFound().build();
        }
        catalog.put(id, item);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/{id}", consumes={"application/json","application/xml"})
    public ResponseEntity deleteItem(@PathVariable int id){
        if(!catalog.containsKey(id)){
            return ResponseEntity.notFound().build();
        }
        catalog.remove(id);
        return ResponseEntity.ok().build();
    }
}
