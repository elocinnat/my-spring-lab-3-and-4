package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class CartRestController {

    @Autowired
    private Map<Integer, Item> catalog;

    @Autowired
    private CartService service;

    @GetMapping(value = "/items", produces={"application/json","application/xml"})
    public List<Item> getAllItems(){
        List<Item> cartItems = new ArrayList<>();
        Map<Integer, Integer> cart = service.getAllItemsInCart();
        for(Map.Entry<Integer, Integer> item : cart.entrySet()){
            int id = item.getKey();
            Item i = catalog.get(id);
            cartItems.add(i);
        }
        return cartItems;
    }

    @GetMapping(value = "/cartCost", produces = {"application/json", "application/xml"})
    public double getCartCost(){
        return service.calculateCartCost();
    }

    @GetMapping(value = "/quantityForItem/{itemId}", produces = {"application/json", "application/xml"})
    public int getQuantityForItem(@PathVariable int itemId){
        Integer quantity = service.getAllItemsInCart().get(itemId);
        if(quantity != null){
            return quantity;
        }
        return 0;
    }

}
